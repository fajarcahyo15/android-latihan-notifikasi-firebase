package io.github.fajarcahyo15.realtime_notification.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by root on 4/7/18.
 */

public class SharedPreferencesManager {

    private static final String PREF_NAME = "DataUser";
    private static final String KEY_AKSES = "nama";
    private SharedPreferences sharedPreferences;


    public SharedPreferencesManager(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public void setAkses(String akses) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_AKSES, akses);
        editor.commit();
    }

    public String getAkses() {
        return sharedPreferences.getString(KEY_AKSES, "");
    }



}
