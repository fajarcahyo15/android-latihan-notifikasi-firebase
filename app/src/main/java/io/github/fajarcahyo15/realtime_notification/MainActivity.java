package io.github.fajarcahyo15.realtime_notification;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import io.github.fajarcahyo15.realtime_notification.helper.SharedPreferencesManager;
import io.github.fajarcahyo15.realtime_notification.presenter.MainPresenter;

public class MainActivity extends AppCompatActivity {

    private static final String USER_PREFS = "DataUser";


    private Button btn_akses, btn_log_token;
    private Spinner spin_akses;
    private TextView tv_akses;
    private LinearLayout lo_utama;
    private MainPresenter mainPresenter;
    private SharedPreferencesManager sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initiateComponent();
        mainPresenter = new MainPresenter(this);
        sharedPreferences = new SharedPreferencesManager(this);
        spin_akses.setAdapter(mainPresenter.getSpinnerItem());

        FirebaseMessaging.getInstance().subscribeToTopic("semua");

        if (mainPresenter.checkAkses()) {
            tv_akses.setText("Akses saat ini: "+sharedPreferences.getAkses());
            spin_akses.setSelection(mainPresenter.getSpinnerIndex(mainPresenter.getSpinnerItem(),sharedPreferences.getAkses()));
        } else {
            Snackbar.make(lo_utama, "Tidak memiliki hak akses",Snackbar.LENGTH_LONG).show();
            tv_akses.setText("Tidak memiliki hak akses");
        }

        setActionButtonLogToken();
        setActionButtonAkses(sharedPreferences);


    }

    // inisialisasi component
    private void initiateComponent() {
        btn_akses = findViewById(R.id.btn_akses);
        btn_log_token = findViewById(R.id.btn_log_token);
        spin_akses = findViewById(R.id.spin_akses);
        tv_akses = findViewById(R.id.tv_akses);
        lo_utama = findViewById(R.id.lo_utama);
    }

    // Aksi Tombol btn_log_token
    private void setActionButtonLogToken() {
        btn_log_token.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("TAG", "InstanceID Token : " + FirebaseInstanceId.getInstance().getToken());
                Toast.makeText(MainActivity.this, "InstanceID Token : \n"+FirebaseInstanceId.getInstance().getToken(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Aksi Tombol btn_akses
    private void setActionButtonAkses(final SharedPreferencesManager sharedPreference) {

        // Aksi Button Akses
        btn_akses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String akses = String.valueOf(spin_akses.getSelectedItem()).toLowerCase();
                String pesan = mainPresenter.setSharedPreferences(akses);

                if (!pesan.equals("")) {
                    Snackbar.make(lo_utama, pesan, Snackbar.LENGTH_LONG).show();
                    tv_akses.setText("Akses saat ini: "+akses);
                } else {
                    Snackbar.make(lo_utama, "Tidak memiliki hak akses", Snackbar.LENGTH_LONG).show();
                    tv_akses.setText("Tidak memiliki akses");
                }

                sharedPreference.setAkses(akses);
            }
        });
    }


}
