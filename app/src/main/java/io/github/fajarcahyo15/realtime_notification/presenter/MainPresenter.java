package io.github.fajarcahyo15.realtime_notification.presenter;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.github.fajarcahyo15.realtime_notification.R;
import io.github.fajarcahyo15.realtime_notification.helper.SharedPreferencesManager;

/**
 * Created by root on 4/7/18.
 */

public class MainPresenter {

    Context context;
    TextView tv_akses;
    Button btn_log_token, btn_akses;
    Spinner spin_akses;
    LinearLayout lo_utama;
    SharedPreferencesManager sharedPreferencesManager;

    public MainPresenter(Context context) {
        this.context = context;

        // Inisialisasi Component
        tv_akses = ((Activity) context).findViewById(R.id.tv_akses);
        btn_log_token = ((Activity) context).findViewById(R.id.btn_log_token);
        btn_akses = ((Activity) context).findViewById(R.id.btn_akses);
        spin_akses = ((Activity) context).findViewById(R.id.spin_akses);
        lo_utama = ((Activity) context).findViewById(R.id.lo_utama);
        sharedPreferencesManager = new SharedPreferencesManager(context);
    }

    // Setting Item Spinner
    public ArrayAdapter<String> getSpinnerItem() {
        List<String> list = new ArrayList<>();
        list.add("Fajar");
        list.add("Ramadan");
        list.add("Jaka");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return dataAdapter;
    }

    // setting shared preferences
    public String setSharedPreferences(String akses) {
        switch (akses){
            case "fajar":
                FirebaseMessaging.getInstance().unsubscribeFromTopic("ramadan");
                FirebaseMessaging.getInstance().unsubscribeFromTopic("jaka");
                FirebaseMessaging.getInstance().subscribeToTopic(akses);
                return "Hak akses menjadi "+akses;

            case "ramadan":
                FirebaseMessaging.getInstance().unsubscribeFromTopic("fajar");
                FirebaseMessaging.getInstance().unsubscribeFromTopic("jaka");
                FirebaseMessaging.getInstance().subscribeToTopic(akses);
                return "Hak akses menjadi "+akses;

            case "jaka":
                FirebaseMessaging.getInstance().unsubscribeFromTopic("fajar");
                FirebaseMessaging.getInstance().unsubscribeFromTopic("ramadan");
                FirebaseMessaging.getInstance().subscribeToTopic(akses);
                return "Hak akses menjadi "+akses;

            default:
                return "";
        }
    }

    // cek akses
    public Boolean checkAkses() {

        Boolean punya = false;

        if (!sharedPreferencesManager.getAkses().equals("")) {
            punya = true;
        }

        return punya;
    }

    public int getSpinnerIndex(ArrayAdapter adapter, String myString){

        int index = 0;

        for (int i=0; i<adapter.getCount(); i++){
            if (myString.trim().equals(adapter.getItem(i).toString().toLowerCase())){
                index = i;
            }


        }
        return index;
    }


}
